<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'=>'admin',
                'email'=>'admin@gmail.com',
                'password'=>bcrypt('password'),
                'email_verified_at'=>now(),
                'user_type'=>'admin',
                'active'       =>  '1',
                'created_at'   =>  now(),
                'updated_at'   =>  now(),
            ],
            [
                'name'=>'teacher',
                'email'=>'teacher@gmail.com',
                'password'=>bcrypt('password'),
                'email_verified_at'=>now(),
                'user_type'=>'teacher',
                'active'       =>  '1',
                'created_at'   =>  now(),
                'updated_at'   =>  now(),
            ],
            [
                'name'=>'user',
                'email'=>'user@gmail.com',
                'password'=>bcrypt('password'),
                'email_verified_at'=>now(),
                'user_type'=>'student',
                'active'       =>  '1',
                'created_at'   =>  now(),
                'updated_at'   =>  now(),
            ]
        ]);
    }
}
