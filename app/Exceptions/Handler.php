<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (NotFoundHttpException $exception,$request) {
            if($request->is('api/*'))
            {
                $statusCode = $exception->getStatusCode($exception);
                if ($statusCode === 404)
                {
                    return res($statusCode, 'URL Not Found');
                }
            }
        });

        
    }
    
    /**
     * Convert a validation exception into a JSON response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Validation\ValidationException  $exception
     * @return \Illuminate\Http\JsonResponse
     */
    // public function render($request, Throwable $exception)
    // {
        // if ($request->expectsJson()) {
            // if ($exception instanceof PostTooLargeException) {
            //     $statusCode = $exception->getStatusCode($exception);
            //     return res($statusCode, "Size of attached file should be less " . ini_get("upload_max_filesize") . "B");
            // }
            
            // if ($exception instanceof ThrottleRequestsException) {
                
            //     $statusCode = $exception->getStatusCode($exception);
            //     return res($statusCode, "Too Many Requests,Please Slow Down");
            
            // }
            // if ($exception instanceof ModelNotFoundException) {

            //     $statusCode = $exception->getStatusCode($exception);
            //     return res($statusCode, 'Entry for ' . str_replace('App\\', '', $exception->getModel()) . ' not found');
            // }

            // if ($exception instanceof QueryException) {

            //     $statusCode = $exception->getStatusCode($exception);
            //     return res($statusCode, 'There was Issue with the Query',$exception);
            // }

            // if ($exception instanceof \Error) {
            //     $statusCode = $exception->getStatusCode($exception);
            //     return res($statusCode, 'There was some internal error',$exception);
            // }
        // }
    // }
    protected function invalidJson($request, ValidationException $exception)
    {
        $errors = [];

        foreach($exception->errors() as $key => $error) 
        {
            $errors[$key] = $error[0];
        }

        return res_failed($exception->getMessage(), [
            'errors' => $errors,
        ]);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $request->expectsJson()
                    ? res(401, $exception->getMessage())
                    : redirect()->guest($exception->redirectTo() ?? route('login'));
    }

}
