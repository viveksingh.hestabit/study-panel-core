<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTeacherProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|string|max:255',
            'address'  => 'required|string',
            'current_school'  => 'required|string',
            'previous_school'  => 'required|string',
            'experience'  => 'required|numeric|gt:0',
            'expertise_subject'  => 'required|string',
            'profile_image'  => 'sometimes|nullable|mimes:jpg,png',
        ];
    }
}
