<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $base_url;
    public $header;
    public function __construct(Request $request)
    {
        $this->base_url = 'http://localhost/study-panel-user/api/';
        $this->header = ['Authorization' => $request->header('Authorization')];
    }
}
