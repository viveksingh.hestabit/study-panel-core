<?php

namespace App\Http\Controllers\API\Teacher;


use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use App\Models\TeacherDetail;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use App\Http\Resources\TeacherResource;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\RefreshTokenRequest;
use App\Http\Requests\TeacherSignupRequest;
use App\Http\Requests\UpdateTeacherProfileRequest;

class LoginController extends Controller
{
    public function get_profile(Request $request)
    {
        $response = Http::teacher()->withHeaders($this->header)->get('getprofile');
        return responseAll($response->getBody());
    }

    public function signup(TeacherSignupRequest $request)
    {
        $response   = Http::attach('profile_image', file_get_contents($request->file('profile_image')), 'profile-image.jpg')
            ->withHeaders($this->header)
            ->post($this->base_url . 'teacher/signUp', $request->all());
        return responseAll($response->getBody());
    }

    public function update_profile(UpdateTeacherProfileRequest $request)
    {
        $http   = Http::withHeaders($this->header);
        if ($request->has('profile_image')) {
            $http->attach('profile_image', file_get_contents($request->file('profile_image')), 'profile-image.jpg');
        }
        $response   = $http->post($this->base_url . 'teacher/updateprofile/detail', $request->all());
        return responseAll($response->getBody());
    }


    public function login(LoginRequest $request)
    {
        $response = Http::teacher()->post('login', $request->all());
        return responseAll($response->getBody());
    }

    public function refreshToken(RefreshTokenRequest $request)
    {
        $response = Http::teacher()->post('token/refresh', $request->all());
        return responseAll($response->getBody());
    }
}
