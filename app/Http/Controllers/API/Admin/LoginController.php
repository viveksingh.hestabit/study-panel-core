<?php

namespace App\Http\Controllers\API\Admin;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\RefreshTokenRequest;

class LoginController extends Controller
{

    public function login(LoginRequest $request)
    {
        $response = Http::admin()->post('login', $request->all());
        return responseAll($response->getBody());
    }

    public function refreshToken(RefreshTokenRequest $request)
    {
        $response = Http::admin()->post('token/refresh', $request->all());
        return responseAll($response->getBody());
    }
}
