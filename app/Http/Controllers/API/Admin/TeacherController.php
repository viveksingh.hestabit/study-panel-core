<?php

namespace App\Http\Controllers\API\Admin;

use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use App\Models\TeacherDetail;
use App\Http\Requests\ValidateUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use App\Http\Resources\TeacherResource;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\TeacherSignupRequest;
use App\Http\Requests\UpdateTeacherProfileRequest;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = Http::admin()->withHeaders($this->header)->get('teachers');
        return responseAll($response->getBody());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherSignupRequest $request)
    {
        $response   = Http::attach('profile_image', file_get_contents($request->file('profile_image')), 'profile-image.jpg')
            ->withHeaders($this->header)
            ->post($this->base_url . 'admin/teachers', $request->all());
        return responseAll($response->getBody());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(TeacherSignupRequest $request, $id)
    {
        $response = Http::admin()->withHeaders($this->header)->get('teachers/' . $id, $request->all());
        return responseAll($response->getBody());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeacherProfileRequest $request, $id)
    {
        $http   = Http::withHeaders($this->header);
        if ($request->has('profile_image')) {
            $http->attach('profile_image', file_get_contents($request->file('profile_image')), 'profile-image.jpg');
        }
        $response   = $http->post($this->base_url . 'admin/teachers/' . $id . '?_method=PUT', $request->all());
        return responseAll($response->getBody());
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = Http::admin()->withHeaders($this->header)->delete('teachers/' . $id, $request->all());
        return responseAll($response->getBody());
    }

    public function approve(ValidateUser $request)
    {
        $response = Http::admin()->withHeaders($this->header)->post('teachers/approve', $request->all());
        return responseAll($response->getBody());
    }
}
