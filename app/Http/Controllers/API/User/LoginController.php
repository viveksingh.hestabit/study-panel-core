<?php

namespace App\Http\Controllers\API\User;

use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use App\Models\StudentDetail;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\RefreshTokenRequest;
use App\Http\Requests\StudentSignupRequest;
use App\Http\Requests\UpdateStudentProfileRequest;

class LoginController extends Controller
{
    public function get_profile(Request $request)
    {
        $response = Http::student()->withHeaders($this->header)->get('getprofile');
        return responseAll($response->getBody());
    }

    public function signup(StudentSignupRequest $request)
    {
        $response   = Http::attach('profile_image', file_get_contents($request->file('profile_image')), 'profile-image.jpg')
            ->withHeaders($this->header)
            ->post($this->base_url . 'user/signUp', $request->all());
        return responseAll($response->getBody());
    }

    public function login(LoginRequest $request)
    {
        $response = Http::student()->post('login', $request->all());
        return responseAll($response->getBody());
    }

    public function update_profile(UpdateStudentProfileRequest $request)
    {
        $http   = Http::withHeaders($this->header);
        if ($request->has('profile_image')) {
            $http->attach('profile_image', file_get_contents($request->file('profile_image')), 'profile-image.jpg');
        }
        $response   = $http->post($this->base_url . 'user/updateprofile/detail', $request->all());
        return responseAll($response->getBody());
    }

    public function refreshToken(RefreshTokenRequest $request)
    {
        $response = Http::student()->post('token/refresh', $request->all());
        return responseAll($response->getBody());
    }
}
